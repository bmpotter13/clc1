# Brandon Potter
#	Adds two numbers provided by user when prompted

.ent main
main:
	li	$v0, 4		# Print string
	la	$a0, first_prompt
	syscall
	li	$v0, 5		# Read word
	syscall
	move	$t0, $v0	# Store result in $t0
				# 

	li	$v0, 4
	la	$a0, second_prompt
	syscall
	li	$v0, 5
	syscall
	move	$t1, $v0

	addu	$t2, $t0, $t1
	li	$v0, 4
	la	$a0, sum_string
	syscall
	
	li	$v0, 1
	move	$a0, $t2
	syscall
	jr 	$ra
.end main

.data
sum_string:	.asciiz "The sum is "
first_prompt:	.asciiz "Enter the first number: "
second_prompt:	 .asciiz "Enter the second number: "
