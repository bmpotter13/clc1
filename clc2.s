# Brandon Potter
#	Adds two numbers provided by user when prompted

.ent main
main:
	li	$v0, 4		# Print string. String addr in $a0
	la	$a0, first_prompt
	syscall
	li	$v0, 5		# Read word. Answer in $v0 
	syscall
	move	$t0, $v0	# Store result in $t0

	li	$v0, 4		# Repeat for second number
	la	$a0, second_prompt
	syscall
	li	$v0, 5
	syscall
	move	$t1, $v0

	sgt	$t3, $t0, $t1	# $t3 = $t0 > $t1 ? 1 : 0
	beq	$t3, 0, less	# If $t0 < $t1, then $t3 = 0
	move	$t2, $t0	# This runs if $t0 > $t1
	b	skip_else	# Copy to $t2, and skip past the 'else' part
less:
	move	$t2, $t1	# This runs from the branch instruction
skip_else:
	li	$v0, 1		# Print integer and then string
	move 	$a0, $t2
	syscall
	li	$v0, 4
	la	$a0, greater_str
	syscall
	
	jr 	$ra		# Return
.end main

.data	# String declarations. Using .asciiz for null ended string
greater_str:	.asciiz " is the larger number."
first_prompt:	.asciiz "Enter the first number: "
second_prompt:	 .asciiz "Enter the second number: "
